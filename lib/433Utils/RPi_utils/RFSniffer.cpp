/*
  RFSniffer

  Usage: ./RFSniffer [<pulseLength>]
  [] = optional

  Hacked from http://code.google.com/p/rc-switch/
  by @justy to provide a handy RF code sniffer
*/

#include "../rc-switch/RCSwitch.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

RCSwitch mySwitch;
string nameFile = "code";

int main(int argc, char *argv[])
{

  // This pin is not the first pin on the RPi GPIO header!
  // Consult https://projects.drogon.net/raspberry-pi/wiringpi/pins/
  // for more information.
  int PIN = 2;

  if (wiringPiSetup() == -1)
  {
    printf("wiringPiSetup failed, exiting...");
    return 0;
  }

  int pulseLength = 0;
  if (argv[1] != NULL)
    pulseLength = atoi(argv[1]);

  mySwitch = RCSwitch();
  if (pulseLength != 0)
    mySwitch.setPulseLength(pulseLength);
  mySwitch.enableReceive(PIN); // Receiver on interrupt 0 => that is pin #2

  while (1)
  {

    ofstream fichier(nameFile.c_str(), ios::out | ios::trunc);
    if (fichier)
    {
      fichier << "0000000" << endl;
    }
    else
    {
      cerr << "Erreur a l'ouverture !" << endl;
    }

    if (mySwitch.available())
    {

      int value = mySwitch.getReceivedValue();

      if (value == 0)
      {
        printf("Unknown encoding\n");
      }
      else
      {

        printf("Received %i\n", mySwitch.getReceivedValue());
        ofstream fichier(nameFile.c_str(), ios::out | ios::trunc);
        if (fichier)
        {
          fichier << mySwitch.getReceivedValue() << endl;
        }
        else
        {
          cerr << "Erreur a l'ouverture ! " << endl;
        }
        usleep(1500000);
      }

      fflush(stdout);
      mySwitch.resetAvailable();
    }
    usleep(100);
  }

  exit(0);
}
