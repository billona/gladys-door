# Gladys DoorScanner 0.1.0

Gladys Doord scans 433 MHz to detect if a door is opened.

Need Gladys version >= 3.4.3. ?

## Documentation

To install this module : 

- Be sure **nmap** is installed on your system. On any Linux system, execute `sudo apt-get install nmap` on your machine.
- Install the module in Gladys
- Reboot Gladys