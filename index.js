module.exports = function (sails) {
    return {
        exec: require('./lib/exec'),
        init: require('./lib/init')
    };
};